trigger AccountAddressTrigger on Account (before insert, before update) {
    if(Trigger.isInsert && Trigger.isBefore || Trigger.isUpdate && Trigger.isBefore){

            AddressMatchingHandler.updateAddress(Trigger.New);
    }
}