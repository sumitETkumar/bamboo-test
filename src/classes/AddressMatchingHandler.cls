public Class AddressMatchingHandler {
    public static void updateAddress(List<Account>addresslistObject){
        for(Account accountObject : addresslistObject){
            if(accountObject.IsDeleted == false){
                accountObject.ShippingStreet = accountObject.BillingStreet;
                accountObject.ShippingCity = accountObject.BillingCity;
                accountObject.ShippingState = accountObject.BillingState;
                accountObject.ShippingPostalCode = accountObject.BillingPostalCode;
                accountObject.ShippingCountry = accountObject.BillingCountry;
            }
        }
    }
}